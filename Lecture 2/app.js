n1 = 9;
n2 = 9.0;
n3 = "9"

console.log(n1);

console.log(n2)

console.log(n2+parseFloat(n3)); // სტრინგ ტიპის არის

console.log(n1/2)

console.log(n1%2) // შეგვიძლია გავიგოთ ნაშთი

console.log(parseInt(n1/2)) // გაყოფის დროს ტოვებს მხოლოდ მთელ ნაწილს





// იკრემენტი და დეკრემენტი - ++n1 ჯერ ზრდის შემდეგ ანიჭებს, n1++ ჯერ ანიჭებს შემდეგ ზრდის

console.log(++n1) // 10
console.log(n1++) // 11
console.log(n1++) // 12
console.log(++n1) // 13
console.log(n1) // 13

//

r = Math.random() // ბეჭდავს რენდომზე რიცხვებს
console.log(r * 10) // ბეჭდავს 0 დან 10 მდე

// 

x = 4.7
console.log(Math.round(x)) // ჩვეულებრივი დამრგვალება
console.log(Math.ceil(x)) // ამრგვალებს ზედამდე
console.log(Math.floor(x)) // ამრგვალებს ქვედამდე
